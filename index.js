
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function welcomeMessage(){	
let firstName=prompt('Enter your first name');
let lastName=prompt('Enter your last name');
let age=prompt('Enter your age');
let location1=prompt('Enter your location')
// console.log(firstName);
// console.log(lastName);
// console.log(age);
console.log('Hi'+firstName+" "+lastName+"!");
console.log(firstName+" "+lastName+"'s"+" ages is "+ age);
console.log(firstName+" "+lastName+" is located at "+ location1);
}
welcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
function top5BandsArtist(){
	let top5=['Airsupply','Behemoth','Disturbed','Metalica','Kiss'];
console.log(top5);
}
top5BandsArtist();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
function top5Movies(){
	console.log("1.Avatar")
	console.log("Tomatometer for Avatar: 82%")
	console.log("2.The Pursuit of Happyness")
	console.log("Tomatometer for The Pursuit of Happyness: 67%" )
	console.log("3.Castaway")
	console.log("Tomatometer for the Castaway: 90%")
	console.log("4.Drops of Jupiter")
	console.log("Tomatometer for the Drops of Jupiter: 56%")
	console.log("5.Pirates of the Caribean")
	console.log("Tomatometer for the Pirates of the Caribean: 80%")
}

top5Movies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
printFriends= function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt ("Enter your first friend's name:"); 
	let friend2 = prompt ("Enter your second friend's name:"); 
	let friend3 = prompt ("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
// console.log("You are friends with:");
// let friend1 = prompt("Enter your first friend's name:"); 
// let friend2 = prompt("Enter your second friend's name:"); 
// let friend3 = prompt("Enter your third friend's name:");

// console.log(friend1);
// console.log(friend2);
// console.log(friend3);